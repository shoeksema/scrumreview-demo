<?php
//require
require 'database/Connection.php';
require 'models/UserModel.php';

//Connection class
$conn = Connection::make();

$users = new UserModel($conn);
$users->allUsers();

//view

require 'views/index.view.php';