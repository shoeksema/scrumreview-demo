<?php

/**
 * Created by: Stephan Hoeksema 2022
 * srcumreview-demo
 */
class Connection
{
    public static function make()
    {
        $host = 'localhost';
        $dbname = 'scrumreview';
        $user = 'root';
        $password = '';

        //https://phpdelusions.net/pdo#dsn

        $dsn = "mysql:host=$host;dbname=$dbname;";
        $options = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        try {
            return new PDO($dsn, $user, $password, $options);
        } catch (PDOException $e) {
            throw new PDOException($e->getMessage(), (int)$e->getCode());
        }
    }

}